# mumble-record
## Mumble Record Bot in NodeJS

# Install
```
yarn or npm i
```
# Run
```
node record.js <server> <nick> <channel>
```
# Example
```
node record.js service.com Root
```
# License
MIT
